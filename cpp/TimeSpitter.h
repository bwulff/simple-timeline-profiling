/* 
 * File:   TimeSpitter.h
 * Author: bwulff
 *
 * Created on September 2, 2015, 10:50 AM
 */

#ifndef TIMESPITTER_H
#define	TIMESPITTER_H

#include <sys/time.h>

class TimeSpitter {

public:
  
  struct timeval s_CurrentTime;
  
  TimeSpitter() {;}
  ~TimeSpitter() {;}
  
  inline void Spit(const char* group, const char* msg)
  {
    gettimeofday (&s_CurrentTime, NULL);
    cerr << s_CurrentTime.tv_sec << "," << s_CurrentTime.tv_usec << "," << group << "," << msg << endl;
  }
  
};

#endif	/* TIMESPITTER_H */

