# Simple Timeline Profiling #

Simple stuff we used in a project to profile timing in a multi-threaded real-time application.

### How to use it ###

The TimeSpitter is a header-only implementation. Just include the header in your source and declare an instance of a ```TimeSpitter```. Then use the ```Spit(group, event_description)``` method to spit out a time mark. The ```gruop``` parameter is used to group events in the chart later on. 

```
#!c++

#include "TimeSpitter.h"

TimeSpitter spitter;

void MyFunction() 
{
  spitter.Spit("myMainThread", "[MyFunction");    // a [ marks the beginning of a time span

  spitter.Spit("myMainThread", "Event!");         // spits without prefix mark singular events

  spitter.Spit("myMainThread", "]MyFunction");    // a ] marks the end of a time span 
}
```

Output is send to ```cerr```. Use ```html/Timeline.html``` to display the chart:

* Copy-and-paste output into the raw data text field
* Click "Convert" to parse the output and wrap the data in JSON
* Click "Load" to display the data in the timeline

### Acknowledgements ###

[VIS.JS](http://visjs.org/) is used to display the timeline.